import {Component} from '@angular/core';
import {Event} from 'src/app/model/event';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent {
  event: Event = new Event(
    1,
    "firstEvent",
    "FinalProject",
    "JavaRo53",
    new Date(2023, 7, 22, 9, 0),
    new Date(2023, 8, 5, 16, 0));


  httpClient: HttpClient;
  route: ActivatedRoute;

  constructor(httpClient: HttpClient, route: ActivatedRoute) {
    this.httpClient = httpClient;
    this.route = route;
  }

  ngOnInit(): void {

    const eventId  = this.route.snapshot.paramMap.get("id"); // sintaxa pentru a accesa parametru din url


// accesam evenimentul cu id-ul 1-response e obiectul ce-l primim noi
    this.httpClient.get("/api/events/"+ eventId).subscribe((response: Object): void => {
      console.log(response)

      this.event = response as Event; // convertim response in Event
      this.event.startDate = new Date(this.event.startDate);
      this.event.endDate = new Date(this.event.endDate);

    });


  }
}

